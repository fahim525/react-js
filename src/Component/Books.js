import React, {Component} from 'react'

import Book from './Book'


class Books extends Component 
{
    render()
    {
      //  console.log(this.props.books)
        return (
            <div>
                {
                    this.props.books.map((book, key)=>{
                       return <Book 
                       deleteHandler={this.props.deleteHandler}
                       changeHandler={this.props.changeHandler}
                       book={book} 
                       key={key} 
                       />
                    })
                }
            </div>
        )
    }
}

export default Books