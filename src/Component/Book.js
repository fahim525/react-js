import React, {Component} from 'react'


class Book extends Component 
{
    state = {
        isEditable : false
    }


    changeKeyHandler = (event) =>
    {
        console.log(event.key)

        if(event.key === 'Enter'){
            this.setState({
                isEditable : false
            })
        }
    }

    render()
    {
        let output = this.state.isEditable ? 
                        <input 
                        type='text' 
                        value={this.props.book.name} 
                        onChange={(e)=>{ this.props.changeHandler(e.target.value, this.props.book.id)  }}
                        onKeyPress={ this.changeKeyHandler }
                        onBlur={ () => this.setState({isEditable: false}) }
                        />
                        :  <p>{this.props.book.name}</p>



        return (
            <div className="container">
                <span onClick={() => this.props.deleteHandler(this.props.book.id)}>X</span>
                <span onClick={ () => this.setState({isEditable: true}) }>Edit</span>

                {output}
                <p>{this.props.book.price}</p>
            </div>
        )
    }
}

export default Book