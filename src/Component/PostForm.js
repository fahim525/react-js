
import React, {Component} from 'react'

const initialState = {
    name: '',
    email: '',
    password: '',
    bio: '',
    country: '',
    gender: '',
    skills: []
}

class PostForm extends Component 
{

    constructor()
    {
        super()
        this.myForm = React.createRef()
    }

    state = {
        ...initialState
    };

    changeHandler = (event) =>
    {
        if(event.target.type === 'checkbox')
        {
            if(event.target.checked)
            {
                this.setState({
                    ...this.state,
                    skills: this.state.skills.concat(event.target.value)
                })
            }else{
                this.setState({
                    ...this.state,
                    skills: this.state.skills.filter(skill => skill !== event.target.value)
                })
            }
        }else{

            this.setState({
                [event.target.name] : event.target.value
            })

        }
        
    }

    submitHandler = event =>
    {
        event.preventDefault()
        console.log(this.state)

        this.myForm.current.reset()

        this.setState({
            ...initialState
        })
    }

    render()
    {
        return (
            <div className='container mt-30'>
                <div className='col-md-6 offset-md-3'>
                    <form ref={ this.myForm } onSubmit={this.submitHandler}>
                        <div className="form-group">
                            <label htmlFor="name">Enter Your Name</label>
                            <input 
                                id="name"
                                name="name"
                                type="text" 
                                className="form-control" 
                                value={this.state.name}
                                onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Enter Your Email Address</label>
                            <input 
                                type="text"
                                name="email"
                                id="email" 
                                className="form-control" 
                                value={this.state.email}
                                onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Enter Your Password</label>
                            <input 
                                type="password"
                                name="password" 
                                id="password" 
                                className="form-control" 
                                value={this.state.password}
                                onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="country">Select Your Country</label>
                            <select name="country"  id="country"  className="form-control"   onChange={this.changeHandler}>
                                <option value='Bangladesh'>Bangladesh</option>
                                <option value='USA'>USA</option>
                                <option value='Australia'>Australia</option>
                                <option value='Pakistan'>Pakistan</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label className='d-block'>Select Your Gender</label>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="gender" type="radio" id="male" value="Male" />
                                <label className="form-check-label" htmlFor="male">Male</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="gender" type="radio" id="female" value="Female"/>
                                <label className="form-check-label" htmlFor="female">Female</label>
                            </div>
                        </div>
                        
                        <div className="form-group">
                            <label className='d-block'>Select Your Skills</label>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="skills" type="checkbox" id="JavaScript" value="JavaScript" />
                                <label className="form-check-label" htmlFor="JavaScript">JavaScript</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="skills" type="checkbox" id="php" value="php"/>
                                <label className="form-check-label" htmlFor="php">PHP</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="skills" type="checkbox" id="React" value="React"/>
                                <label className="form-check-label" htmlFor="React">React</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input onChange={this.changeHandler} className="form-check-input" name="skills" type="checkbox" id="VueJs" value="VueJs"/>
                                <label className="form-check-label" htmlFor="VueJs">VueJs</label>
                            </div>

                        </div>

                        <div className="form-group">
                            <label htmlFor="bio">Enter Your Short Bio</label>
                            <textarea 
                                type="text"
                                name="bio"
                                id="bio"
                                className="form-control" 
                                value={this.state.bio}
                                onChange={this.changeHandler}
                            ></textarea>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        )
    }
}


export default PostForm