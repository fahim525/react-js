import React , {Component} from 'react';

class Counter extends Component 
{
   
    constructor(props)
    {
        super(props)

        this.state = {
            count : 6,
            color : 'black'
        }

    }

    decrease = () => {
        this.setState({
            count: this.state.count - 1
        })
        if(this.state.count <= 5){
            this.setState({
                color: 'red'
            })
        }
    }

    
    increase = () => {
        this.setState({
            count: this.state.count + 1
        })

        if(this.state.count >= 10){
            
            this.setState({
                color: 'green'
            })
        }
    }

   


    render()
    {
        console.log(this.props.name[0].name)
        return (
            <div>
                <h2>{  }</h2>
                <h1 style={{ color: this.state.color }}>
                    <span className="circle" onClick={ this.decrease }> - </span> 
                        <span> {this.state.count} </span> 
                    <span className="circle" onClick={ this.increase }> + </span>
                </h1>
            </div>
        )
    }
}

export default Counter