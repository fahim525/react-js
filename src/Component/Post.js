import React, {Component} from 'react'


class Post extends Component {

    render()
    {
        //console.log(this.props.posts)
        let {posts} = this.props

        if(posts.length === 0)
        {
            return <h1 className="text-center">Loading . . . </h1>
        }else{
           return (
               <div className='container'>
                    <ul className='list-group'>
                        {posts.map(post => <li key={post.id} className='list-group-item'>{ post.title }</li> )}
                   </ul>
               </div>
            )
        }
        
    }
}

export default Post