import React, { Component } from 'react';
//import About from './Component/Aboout';
//import Counter from './Component/Counter';
//import Event from './Component/Event'
//import Style from './Component/Style'


//import Books from './Component/Books'
//import Post from './Component/Post'

import './App.css';
import Axios from 'axios';
import PostForm from './Component/PostForm';

class App extends Component {

  state = {
    books : [
      {id: 1, name: 'JavaScript', price: 20},
      {id: 2, name: 'HTML', price: 25},
      {id: 3, name: 'PHP', price: 22},
      {id: 4, name: 'Angular', price: 18}
    ],

    posts : []
  }

  componentDidMount()
  {
    Axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(response => {
      this.setState({
        posts : response.data
      }) 
        
    })
    .catch(error => console.log(error))
  }



  deleteHandler = (id) =>
  {
    let newBooks = this.state.books.filter(book => book.id !== id)
    this.setState({
      books: newBooks
    })
  }

  changeHandler = (name, id) => 
  {
      let newBooks = this.state.books.map((book)=>{
        if(id === book.id)
        {
          return {
            ...book,
            name
          }
        }
        return book
      })

      this.setState({
        books: newBooks
      })
  }

  render() {
    //console.log(this.state.posts)
    return (
      <div>
       
        {/* <Books 
              deleteHandler={this.deleteHandler.bind(this)}
              changeHandler={this.changeHandler.bind(this)}
              books={this.state.books} />

        <Post posts={this.state.posts} /> */}

        <PostForm />
    </div>
    );
  }
}

export default App;
